# A React Task
- In this task we will create a savable note and learn how to use **localStorage**.

## Objective

- Checkout the dev branch.
- Provide the textare with an **onChange**
- handler and connect it to a state.
- Use the buttons to save and reset the note in the text area.
- On the next page load, the component must get the saved note from the **localStorage**.
- When implemented, whe merge the dev branch to master.

## Requirements

- The project starts with **npm run start**.
- We must use browser`s **localStorage**.
- Must not change the classes of the elements in the boilerplate.

## Gotchas

-More information about **localStorage** - https://developer.mozilla.org/en-US/docs/Web/API/Window/localStorage
