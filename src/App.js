import "./App.css";
import React, { useState } from 'react';
//localStorage saved "this is a text that should be saved";loca


function App() {
  const savedState = localStorage.getItem('state');
  const [state, setState] = useState(savedState ? savedState: '');
  
  function saveLocal() {
    localStorage.setItem('state', state);
    console.log(localStorage.state);
  }
  
 function clearLocal() {
     localStorage.clear(); 
      setState("");
  }
  
  return (
    <div className="App">
      <div className="box">
        <div className="field">
          <div className="control">
            <textarea onChange={(e) => { setState(e.target.value) }}   value={state}  className="textarea is-large" placeholder="Notes..." />
          </div>
        </div>
        <button onClick={saveLocal}  className="button is-large is-primary is-active">Save</button>
        <button onClick={clearLocal} className="button is-large">Clear</button>
      </div>
    </div>
  );
}

export default App;
